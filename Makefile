TARGET	:= extern-mon
SRC_DIR := ./src
OBJ_DIR := ./obj
SRC_FILES := $(wildcard $(SRC_DIR)/*.c)
OBJ_FILES := $(patsubst $(SRC_DIR)/%.c,$(OBJ_DIR)/%.o,$(SRC_FILES))
INCLUDE_FLAGS := -I./include
LD_FLAGS := -lm -lpthread

all: $(TARGET)

$(TARGET): $(OBJ_FILES)
	gcc -o $@ $^ $(LD_FLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	gcc $(INCLUDE_FLAGS) -c -o $@ $<

clean:
	rm -rf $(TARGET) $(OBJ_DIR)/*.o
