#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <inttypes.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <pthread.h>
#include <sys/syscall.h>
#include <linux/perf_event.h>
#include <termios.h>

#define TERMINAL    "/dev/ttyS0"

#define AMD_MSR_PWR_UNIT 0xC0010299
#define AMD_MSR_CORE_ENERGY 0xC001029A
#define AMD_MSR_PACKAGE_ENERGY 0xC001029B

#define AMD_TIME_UNIT_MASK 0xF0000
#define AMD_ENERGY_UNIT_MASK 0x1F00
#define AMD_POWER_UNIT_MASK 0xF
#define STRING_BUFFER 1024

#define MAX_CPUS	32
#define MAX_PACKAGES	16

#define STM 2

const char * STR_APP_NAME			= "cpu-stat";
const char * STR_APP_VERSION			= "0.01.02";

static int total_cores=0,total_packages=0;
static int package_map[MAX_PACKAGES];

struct core_stats{
    float energy_used;
    float freq;
    float usage;
    float total_usage_sample_1;
    float work_usage_sample_1;
    float total_usage_sample_2;
    float work_usage_sample_2;
};

float gpu_temp;
float gpu_freq;
float gpu_usage;
float gpu_vcore;
float gpu_fanrpm;
float gpu_vram;
float gpu_power;
float cpu_temp;
float v_core;


static int detect_packages(void) {

    char filename[BUFSIZ];
    FILE *fff;
    int package;
    int i;

    for(i=0;i<MAX_PACKAGES;i++) package_map[i]=-1;

    //printf("\t");
    for(i=0;i<MAX_CPUS;i++) {
        sprintf(filename,"/sys/devices/system/cpu/cpu%d/topology/physical_package_id",i);
        fff=fopen(filename,"r");
        if (fff==0) break;
        fscanf(fff,"%d",&package);
        //printf("%d (%d)",i,package);
        //if (i%8==7) printf("\n\t"); else printf(", ");
        fclose(fff);

        if (package_map[package] == -1) {
            total_packages++;
            package_map[package]=i;
        }

    }

    //printf("\n");

    total_cores=i;

    //printf("\tDetected %d cores in %d packages\n\n",
    //    total_cores,total_packages);

    return 0;
}

int open_msr(int core) {

    char msr_filename[BUFSIZ];
    int fd;

    sprintf(msr_filename, "/dev/cpu/%d/msr", core);
    fd = open(msr_filename, O_RDONLY);
    if ( fd < 0 ) {
        if ( errno == ENXIO ) {
            fprintf(stderr, "rdmsr: No CPU %d\n", core);
            exit(2);
        } else if ( errno == EIO ) {
            fprintf(stderr, "rdmsr: CPU %d doesn't support MSRs\n",
                    core);
            exit(3);
        } else {
            perror("rdmsr:open");
            fprintf(stderr,"Trying to open %s\n",msr_filename);
            exit(127);
        }
    }

    return fd;
}

long long read_msr(int fd, unsigned int which) {

    uint64_t data;

    if ( pread(fd, &data, sizeof data, which) != sizeof data ) {
        perror("rdmsr:pread");
        exit(127);
    }

    return (long long)data;
}

double rapl_msr_amd_core(int core_count) {
    int i = core_count;
    unsigned int time_unit, energy_unit, power_unit;
    double time_unit_d, energy_unit_d, power_unit_d;

    double *core_energy = (double*)malloc(sizeof(double)*total_cores/STM);
    double *core_energy_delta = (double*)malloc(sizeof(double)*total_cores/STM);

    double *package = (double*)malloc(sizeof(double)*total_cores/STM);
    double *package_delta = (double*)malloc(sizeof(double)*total_cores/STM);

    int *fd = (int*)malloc(sizeof(int)*total_cores/STM);

    for (int i = 0; i < total_cores/STM; i++) {
        fd[i] = open_msr(i*2);
    }

    int core_energy_units = read_msr(fd[0], AMD_MSR_PWR_UNIT);

    time_unit = (core_energy_units & AMD_TIME_UNIT_MASK) >> 16;
    energy_unit = (core_energy_units & AMD_ENERGY_UNIT_MASK) >> 8;
    power_unit = (core_energy_units & AMD_POWER_UNIT_MASK);

    time_unit_d = pow(0.5,(double)(time_unit));
    energy_unit_d = pow(0.5,(double)(energy_unit));
    power_unit_d = pow(0.5,(double)(power_unit));

    int core_energy_raw;
    int package_raw;
    // Read per core energy values

    core_energy_raw = read_msr(fd[i], AMD_MSR_CORE_ENERGY);
    package_raw = read_msr(fd[i], AMD_MSR_PACKAGE_ENERGY);

    core_energy[i] = core_energy_raw * energy_unit_d;
    package[i] = package_raw * energy_unit_d;

    usleep(100000);
    core_energy_raw = read_msr(fd[i], AMD_MSR_CORE_ENERGY);
    package_raw = read_msr(fd[i], AMD_MSR_PACKAGE_ENERGY);

    core_energy_delta[i] = core_energy_raw * energy_unit_d;
    package_delta[i] = package_raw * energy_unit_d;

    double sum = 0;
    double diff = (core_energy_delta[i] - core_energy[i])*10;

    free(core_energy);
    free(core_energy_delta);
    free(package);
    free(package_delta);
    //free(fd);
    for (int i = 0; i < total_cores/STM; i++) {
        close(fd[i]);
    }

    return diff;
}

void *utilization_i(struct core_stats *core_id)
{
	char c;
	int id, user, nice, system, idle, iowait, irq, softirq, steal, guest, guest_nice;
	FILE *stat;
	while(1)
		{
		stat = fopen("/proc/stat", "r");
		for(int i=0; i <= total_cores; i++)
		{
			if(i>0)
			{
				fscanf(stat,"cpu%d %d %d %d %d %d %d %d %d %d %d\n", &id, &user, &nice, &system, &idle, &iowait, &irq, &softirq, &steal, &guest, &guest_nice);
				core_id[id].total_usage_sample_1 =  (float)(user + nice + system + idle + iowait + irq + softirq + steal + guest + guest_nice);
				core_id[id].work_usage_sample_1 = core_id[id].total_usage_sample_1 - ((float)idle + (float)iowait);
			}
			else
			{
				fscanf(stat,"cpu %d %d %d %d %d %d %d %d %d %d\n", &user, &nice, &system, &idle, &iowait, &irq, &softirq, &steal, &guest, &guest_nice);
			}
		}
		fclose(stat);
		sleep(1);
		stat = fopen("/proc/stat", "r");
		for(int i=0; i <= total_cores; i++)
		{
			if(i>0)
			{
				fscanf(stat,"cpu%d %d %d %d %d %d %d %d %d %d %d\n", &id, &user, &nice, &system, &idle, &iowait, &irq, &softirq, &steal, &guest, &guest_nice);
				core_id[id].total_usage_sample_2 =  (float)(user + nice + system + idle + iowait + irq + softirq + steal + guest + guest_nice);
				//core_id[id].work_usage_sample_2 = (float)idle / core_id[id].total_usage_sample_2;
				core_id[id].work_usage_sample_2 = core_id[id].total_usage_sample_2 - ((float)idle + (float)iowait);
				core_id[id].usage = ((core_id[id].work_usage_sample_2 - core_id[id].work_usage_sample_1)/(core_id[id].total_usage_sample_2 - core_id[id].total_usage_sample_1)) * 100.0;
				if (core_id[id].usage < 0)
				{
					core_id[id].usage = 0.0;
				}
				//core_id[id].usage = (100.0 - core_id[id].work_usage_sample_2);
			}
			else
			{
				fscanf(stat,"cpu %d %d %d %d %d %d %d %d %d %d\n", &user, &nice, &system, &idle, &iowait, &irq, &softirq, &steal, &guest, &guest_nice);
			}
		}
		fclose(stat);
	}

}

void *cpufreq_i(struct core_stats *core_id)
{
    char filename[BUFSIZ];
    int freq_i;
    FILE *fp;
    while(1)
    {
		for(int i = 0; i < total_cores; i++)
		{
			sprintf(filename,"/sys/devices/system/cpu/cpu%d/cpufreq/scaling_cur_freq",i);
			fp=fopen(filename,"r");
			fscanf(fp,"%d", &freq_i);

			fclose(fp);

			core_id[i].freq = ((double)freq_i/1000.0);
		}
		sleep(1);
    }

}

void *get_energy(struct core_stats *core_id)
{
	while(1)
		{
		for(int i = 0; i < total_cores; i++)
		{
			core_id[i].energy_used = rapl_msr_amd_core(i/2);
			core_id[i+1].energy_used = core_id[i].energy_used;
		}
	}
}

void *get_misc()
{
	FILE *fp;

	while(1)
	{
		fp=fopen("/sys/class/drm/card0/device/hwmon/hwmon1/temp1_input","r");
		fscanf(fp,"%f", &gpu_temp);
		fclose(fp);

		fp=fopen("/sys/class/drm/card0/device/hwmon/hwmon1/freq1_input","r");
		fscanf(fp,"%f", &gpu_freq);
		fclose(fp);

		fp=fopen("/sys/class/drm/card0/device/gpu_busy_percent","r");
		fscanf(fp,"%f", &gpu_usage);
		fclose(fp);

		fp=fopen("/sys/class/drm/card0/device/hwmon/hwmon1/fan1_input","r");
		fscanf(fp,"%f", &gpu_fanrpm);
		fclose(fp);

		fp=fopen("/sys/class/drm/card0/device/hwmon/hwmon1/in0_input","r");
		fscanf(fp,"%f", &gpu_vcore);
		fclose(fp);

		fp=fopen("/sys/class/drm/card0/device/mem_info_vram_used","r");
		fscanf(fp,"%f", &gpu_vram);
		fclose(fp);

		fp=fopen("/sys/class/drm/card0/device/hwmon/hwmon1/power1_average","r");
		fscanf(fp,"%f", &gpu_power);
		fclose(fp);

		fp=fopen("/sys/class/hwmon/hwmon2/temp1_input","r");
		fscanf(fp,"%f", &cpu_temp);
		fclose(fp);

		fp=fopen("/sys/class/hwmon/hwmon2/in0_input","r");
		fscanf(fp,"%f", &v_core);
		fclose(fp);

		gpu_temp = gpu_temp / 1000.0;
		gpu_freq = gpu_freq / 1000000.0;
		gpu_vcore = gpu_vcore / 1000.0;
		gpu_vram = gpu_vram / 1000000.0;
		cpu_temp = cpu_temp / 1000.0;
		v_core = v_core / 1000.0;
		gpu_power = gpu_power / 1000000.0;

		sleep(1);
	}
}

int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 1;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

void set_mincount(int fd, int mcount)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error tcgetattr: %s\n", strerror(errno));
        return;
    }

    tty.c_cc[VMIN] = mcount ? 1 : 0;
    tty.c_cc[VTIME] = 5;        /* half second timer */

    if (tcsetattr(fd, TCSANOW, &tty) < 0)
        printf("Error tcsetattr: %s\n", strerror(errno));
}

int main(int argc, char *argv[])
{
	detect_packages();
	struct core_stats core_id[total_cores];
	char serial_out[3000];

	pthread_t thread_id1, thread_id2, thread_id3, thread_id4;
	pthread_create(&thread_id1, NULL, utilization_i, core_id);
	pthread_create(&thread_id2, NULL, get_energy, core_id);
	pthread_create(&thread_id3, NULL, cpufreq_i, core_id);
	pthread_create(&thread_id4, NULL, get_misc, NULL);

	//Serial stuff
	char *portname = TERMINAL;
	int fd;
	int wlen;
	fd = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
	set_interface_attribs(fd, B115200);
	//Serial stuff ends

	sleep(5);
	while(1)
	{

		for(int i = 0; i < total_cores; i++)
		{
			sprintf(serial_out + strlen(serial_out),"Core %d Usage\t%.2f \n\r", i, core_id[i].usage);
			sprintf(serial_out + strlen(serial_out),"Core %d Energy\t%.2f\n\r", i, core_id[i].energy_used);
			sprintf(serial_out + strlen(serial_out),"Core %d Freq\t%.2f \n\r", i, core_id[i].freq);
		}
		sprintf(serial_out + strlen(serial_out),"CPU Temp\t%.2f\n\r", cpu_temp);
		sprintf(serial_out + strlen(serial_out),"CPU Vcore\t%.2f\n\r", v_core);
		sprintf(serial_out + strlen(serial_out),"GPU Temp\t%.2f\n\r", gpu_temp);
		sprintf(serial_out + strlen(serial_out),"GPU Freq\t%.2f\n\r", gpu_freq);
		sprintf(serial_out + strlen(serial_out),"GPU Usage\t%.2f\n\r", gpu_usage);
		sprintf(serial_out + strlen(serial_out),"GPU RPM \t%.2f\n\r", gpu_fanrpm);
		sprintf(serial_out + strlen(serial_out),"GPU Vcore\t%.2f\n\r", gpu_vcore);
		sprintf(serial_out + strlen(serial_out),"GPU VRAM\t%.2f\n\r", gpu_vram);
		sprintf(serial_out + strlen(serial_out),"GPU Energy\t%.2f\n\r", gpu_power);

		printf("%s", serial_out);

		wlen = write(fd, serial_out, strlen(serial_out));
		tcdrain(fd);

		sprintf(serial_out, NULL);

		sleep(1);

	}
    return 0;
}
